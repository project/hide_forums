<?php
/**
 * @file
 * Provides administrative page callback implementations.
 */

/**
 * Overriding system settings form.
 */
function hide_forums_system_settings_form($form) {
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#submit'][] = 'system_settings_form_submit';
  $form['#theme'] = 'system_settings_form';
  return $form;
}


/**
 * Form builder for the settings form.
 */
function hide_forums_admin_settings() {
  
  $vid = variable_get('forum_nav_vocabulary', '');
  $tags = taxonomy_get_tree($vid);

  $options = array();
  foreach ($tags as $tag) {
    switch ($tag->depth) {
      case 0:
	$prefix = '';
	break;
      case 1:
	$prefix = '- ';
	break;
      case 2:
	$prefix = '-- ';
	break;
      case 3:
	$prefix = '--- ';
	break;
    }
    $options[$tag->tid] = $prefix. $tag->name;
  }
  
  $form['hide_forums_selected'] = array(
    '#type' => 'select',
    '#title' => t('Select forums'),
    '#options' => $options,
    '#multiple' => TRUE,
    '#size'=> 20,
    '#default_value' => variable_get('hide_forums_selected', array()),
    '#description' => t('Select one or more forums that need to be hidden'),
  );
  
  return hide_forums_system_settings_form($form);
}

/**
 * Validation handler for the settings form.
 */
function hide_forums_admin_settings_validate($form, &$form_state) {

}
